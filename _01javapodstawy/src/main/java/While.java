import java.util.Scanner;

public class While {
    public static void main(String[] args) {

        boolean warunekCiaglosci = true;
        int i = 0;

        while (warunekCiaglosci) {
            i++;
            if (i >= 10) {
                break;
            }
            System.out.println(i);
        }

        i = 0;
        while (i++ <= 10) {
            System.out.println(i);
        }

        Scanner naszSkaner = new Scanner(System.in);
        while (true) {
            //byte, short, int, long, enum, String
            byte opcjaMenu = naszSkaner.nextByte();
            char znak = '0';
            switch (opcjaMenu) {
                case 1: {
                    znak = '\u269D';
                    break;
                }
                case 2: {
                    znak = '\u2694';
                    break;
                }
                case 3: {
                    znak = '\u2691';
                    break;
                }
                case 4: {
                    znak = '\u2685';
                    break;
                }
                case 5: {
                    znak = '\u2621';
                    break;
                }
                default: {
                    System.out.println("Wybrano nieprawidłową opcję");
                    continue;
                }
            }

            System.out.println(znak);
            break;
        }

        //> Wypisz wszystkie liczby z zakresu n ... 1 (malejąco)
        int liczba = naszSkaner.nextInt();
        while(liczba >= 1){
            System.out.println(liczba);
            liczba--;
        }


    }

}
