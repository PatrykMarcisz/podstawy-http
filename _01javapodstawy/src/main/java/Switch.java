import java.util.Scanner;

public class Switch {

    public static void main(String[] args){
        //byte, short, int, long, enum, String
        Scanner naszSkaner = new Scanner(System.in);
        byte opcjaMenu = naszSkaner.nextByte();
        char znak = '0';
        switch (opcjaMenu){
            case 1: {
                znak = '\u269D';
                break;
            }
            case 2: {
                znak = '\u2694';
                break;
            }
            case 3: {
                znak = '\u2691';
                break;
            }
            case 4: {
                znak = '\u2685';
                break;
            }
            case 5: {
                znak = '\u2621';
                break;
            }
            default: {
                System.out.println("Wybrano nieprawidłową opcję");
                znak = '0';
            }
        }
        System.out.println(znak);

        String imie = "Patryk";
        switch(imie){
            case "Patryk" : {
                System.out.println("czesc Patryk");
            }
            case "Anna" : {
                System.out.println("czesc Anna");
            }
            default: {
                System.out.println("nie rozpoznano imienia");
            }
        }

    }

}
