public class PodzielnaPrzez3i5 {
    public static void main(String[] args) {

        int i = 21;

        if ((i % 3 == 0) && (i % 7 == 0)) {
            System.out.println("podzielna przez 3 i 7");
        } else if (i % 3 == 0) {
            System.out.println("podzielna przez 3");
        } else if (i % 7 == 0) {
            System.out.println("podzielna przez 7");
        }
    }
}
