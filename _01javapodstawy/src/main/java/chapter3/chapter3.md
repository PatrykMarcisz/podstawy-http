Kursanci poznają:

- klasę String i kilka jej podstawowych metod
- co znaczy, że klasa jest immutable
- inne metody System.out (print, printf) / String.format
Kursanci potrafią:
- tworzyć instancję klasy używając konstruktora oraz skrótu ""
- łączyć napisy używając operatora "+".
- wyświetlić napisy za pomocą System.out.print(), System.out.println() czy
System.out.printf().
- wyświetlić liczbę zmiennoprzecinkową z ograniczoną dokładnością za
pomocą printf (%.2f)
- porównywać napisy za pomocą metody equals
- podać różnicę pomiędzy "" (empty String) a null
- opcjonalnie: można wprowadzić klasę Scanner i pokazać, jak odczytywać
wejście z klawiatury. Bez szczegółowego tłumaczenie InputStream
- opcjonalnie: używać metod klasy StringBuilder