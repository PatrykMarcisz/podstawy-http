![](zmienna.png)

### Prymitywne typy w Javie

| typ danych | rozmiar | zakres | domyślna wartość |
|-----|-----|-----|-----|
|byte|1 bajt| -128 do 127 | 0 |
|short|2 bajty|-32 768 do 32 767| 0 |
|int|4 bajty|-2 147 483 648 do 2 147 483 647|0|
|long|8 bajtow|-2^63 do (2^63)-1| 0L|
|float|4 bajty| -3.4E+38 do +3.4E+38 (precyzja 7 cyfr)|0.0f|
|double|8 bajtow|-1.7E+308 do +1.7E+308 (precyzja 16 cyfr)|0.0d|
|boolean|1 bit|{true, false}|false|
|char|2 bajty|\u0000' do '\uffff' (0, 65 535>|'\u0000'|
|String|2 bajty * ilość znaków| zbiór obiektów typu char | null |
>1 bajt = 8 bitów

#### Literał:
> ```
> fragment kodu programu, który może być przypisany do zmiennej i nie jest wywołaniem konstruktora   
> ani złożoną instrukcją. 
> ``` 

##### Literały dla liczb całkowitych:
* domyślnym typem dla literałów typu całkowitego jest int, co oznacza że kompilator domyślnie traktuje   
liczbę wpisaną w program jako int.
    ```java
        int val = 10; 
    ```
* w przypadku typów o zakresie mniejszym niż int, następuje automatyczna konwersja na poziomie kompilatora
    ```java
      short shortVal = 10;
  
        //w rzeczywistosci 
  
      short shortVal = (short)10;    
    ```
* liczby całkowite możemy zapisywać w jednym z trzech formatów:
    * liczba dziesiętna (decimal): dostępne cyfry z przedziału 0-9,  
     najbardziej naturalny bo używany na co dzień przez każdego z nas
        ```java
        int decVal = 26;
        ```
    * liczba szesnastkowa (hexadecimal): dostępne cyfry z przedziału 0-9 oraz litery A-F,
        ```java
        int hexVal = 0x1a; //26
        ```
    * liczba dwójkowa (binary): dostępne cyfry to 0 oraz 1 (bit)
        ```java
        int binVal = 0b11010; //26
       ```
* liczby całkowite typu long, muszą kończyć się przyrostkiem l lub L, jednakże rekomendowane jest użycie L,  
 ponieważ l może być często mylone z 1, szczególnie w przypadku gdy używamy czcionki o stałej szerokości

##### Literały dla liczb zmiennoprzecinkowych
* domyślna konwersja typu dziesiętnego na typ zmiennoprzecinkowy
* domyslny literał dla liczb zmiennoprzecinkowych (np. 20.34) to double (opcjonalnie możemy dopisać d lub D)
```java
double d1 = 123.4;
double d2 = 123.4D;
```
* literały typu float kończą się na f lub F (jak w przypadku int-long).
```java
float f1  = 123.4f;
```
* możemy używać potęg dziesiętnych przy pomocy e lub E
```java
double d2 = 1.234e2; //to samo co d1, możemy to zapisać jako 1.234 * 10^2 = 1.234 * 100 = 123.4
```
* części dziesiętne oddzielane są kropką  "."  !

> Od Javy 7, możliwe jest umieszczanie znaków _ pomiędzy cyframi, w celu zwiększenia czytelności,  
> np. zamiast 12345678 możemy zapisać 12_345_678
##### Literały dla typu boolean
* boolean oznacza wartość logiczną, prawda albo fałsz
* może przyjmować tylko dwie wartości
    * true
    ```boolean b1 = true; ```
    * false
    ```boolean b2 = false;```
    
##### Literały dla typu znakowego (char) i ciągu znaków (String)
Zarówno w przypadku String jak i char, możemy przypisywać znaki typu UNICODE (UTF-16). Jeżeli 

> #### zadanie
> Zadeklaruj po jednej zmiennej każdego typu prostego, przypisz im wartości, a następnie wypisz je na konsolę 
> ```java
> System.out.println( __zmienna__ );
> ```



[źródło](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html)  

[](https://chortle.ccsu.edu/java5/Notes/chap11/ch11_3.html)