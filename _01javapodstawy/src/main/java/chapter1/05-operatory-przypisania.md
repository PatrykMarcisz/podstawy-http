#### Operatory przypisania

```=``` - operator przypisania wartości wyrażenia po prawej jego stronie do zmiennej znajdującej się po lewej stronie  
np. ```int x = 10;```

###```+=``` 
operator rozszerzający przypisanie o sumę, bieżąca wartość zmiennej + wartość wyrażenia tzn. najpierw obliczana  
 jest operacja po prawej stronie, a następnie dodawan do bieżącej wartości zmiennej.
```java
int x = 5;
x += 5; 
```
jest tożsame z 
```java
int x = 5;
x = x + 5;
```
np. 
```java 
int x = 10; 
x += 5 + 15;   // wartosc po prawej stronie: 20, wartość po lewej: 10; po operacji x = 20 + 10 = 30;
``` 

### ```-=```
operator rozszerzający przypisanie o różnicę, analogiczny do ``+=``,
np. 
```java
int x = 5;
x -= 5;
```

jest tożsame z 
```java
int x = 5;
x = x - 5;
```

### ``` *= ``` 
operator przypisania rozszerzony o iloczyn
np. 
```java
int x = 10;
x *= 2 + 5;
```

jest tożsame z 
```java
int x = 10;
x = x * (2+5);
```
Zadanie:   jaka jest wartość poniższego wyrażenia ? 
```java
int x = 10;
x *= 2 + 5 * 3;
```
### ``` /= ```
Operator przypisania rozszerzony o iloraz
np. 

```java
int x = 10;
x /= 2;
```

jest tożsame z 
```java
int x = 10;
x = x / 2;
```

### ``` %= ```
Operator przypisania rozszerzony o modulo (reszta z dzielenia)
np. 

```java
int x = 12;
x %= 5;
```

jest tożsame z 
```java
int x = 12;
x = x % 5;
```

#### ___Zadanie___:
jaką wartość przyjmą następujące wyrażenia w poszczególnych liniach?   
```java
int x = 5;
x += 5;
x -= 3;
x *= 10
x /= 2;
x += x;
x /= 7;
x += x + 15;
x += x++;
x += ++x;
```