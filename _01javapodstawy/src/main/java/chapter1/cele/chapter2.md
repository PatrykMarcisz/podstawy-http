Kursanci poznają:
- instrukcje warunkowe (if, else if, else, switch)
Kursanci potrafią:
- używać instrukcji warunkowych w konkretnych zastosowaniach i wybrać
najbardziej odpowiednie
- deklarować zmienne w odpowiednim zakresie (zasięg blokowy) - na to
należy położyć bardzo duży nacisk, bo zdecydowana większość kursantów
ma z tym problem na późniejszych etapach.