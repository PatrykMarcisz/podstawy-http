Kursanci poznają:
- typy danych w Javie (prymitywne i obiektowe). W szczególności więcej
czasu poświęcić na: int, double, boolean, String
- różne typy operatorów (tj. przypisania, relacji, arytmetyczne, logiczne)

Kursanci potrafią:
- wybrać odpowiedni typ do przechowania konkretnych danych
- tworzyć, inicjalizować i używać zmiennych oraz stałych
- rzutować w górę i w dół
- bezpiecznie rzutować w dół i używać instanceof
- dokonywać dzielenia liczb całkowitych aby dostać liczbę ułamkową
(castowanie do double/float)
- Korzystać z operatora modulo
- Wskazać różnicę między preinkrementacją a postinkrementacją