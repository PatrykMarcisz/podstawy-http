#### Operatory matematyczne
pozostając nadal w sferze typów prostych (zwłaszcza tych liczbowych) warto również wspomnieć o operacjach matematycznych. 

Do naszej dyspozycji zostały oddane następujące operatory:

| Operacja | operator | 
|-----|-----|
|dodawanie| + |
|odejmowanie| - |
|mnożenie| * |
|dzielenie| / |
|reszta z dzielenia (modulo)| % |
|zmiana znaku| - [ zmienna ] |

#### dodawanie
```java
int a = 10;
int b = 20;
int c = a + b;
```

#### odejmowanie
```java 
int a = 10;
int b = 20;
int c = a - b;
```

#### mnożenie
```java
int a = 10;
int b = 20;
int c = a * b;
```

#### dzielenie
````java
int a = 10;
int b = 20;
int c = a / b;
````

tutaj jednakże mamy trochę inne zasady.
w przypadku działania 10/20 otrzymamy 0.  
Dlaczego ? Otóż tak jak było wspomniane, typ int przechowuje tylko typ całkowity,  
 stąd wynikiem dzielenia liczby całkowitej przez liczbę całkowitą będzie część całkowita wyniku  
jako że 10/20 = 0.5, to część całkowita = 0

Czy to znaczy że możemy dzielić tylko liczby całkowite ?   
w żadnym razie, wystarczy że przypiszemy wynik działania do zmiennej typu double lub float

```java
int a = 10;
int b = 20;
double c = a/b;

System.out.println(c); 
```

> Hej, zaraz przecież wydruk to 0.0, dlaczego ?

przypomnijmy sobie jak działa przypisanie wartości:
* najpierw obliczana jest wartość wyrażenia po prawej stronie
* następnie wartość ta przypisywana jest do zmiennej znajdującej się po lewej stronie operacji przypisania

w związku z tym, najpierw obliczana jest wartość wyrażenia  
``` a / b = (int)a / (int)b = 0 ```,  
a następnie przypisywana jest ona do zmiennej typu double, a za sprawą automatycznej konwersji,  
 0 jest automatycznie konwertowane do 0.0

___Co mogę w takiej sytuacji zrobić ?___  
Jeśli przynajmniej jedna z liczb biorących udział w operacji jest liczbą zmiennoprzecinkową,  
 cała operacja również zostanie sprowadzona do "ułamka"

```java
double c = a / (double)b; 

System.out.println(c);  //wynik: 0.5
```
---
Reszta z dzielenia
``` 
int a = 10;
int b = 3;
int c = a % b; //reszta z dzielenia 10 przez 3 = 1 (3*3+1=10)

System.out.println(c); //Wynik: 1
```
---

dodatkowo dostepne są dwa operatory dodawania i odejmowania (modyfikacji wartości o 1 dokładnie rzecz ujmując)
* inkrementacja
    * post - operacja zwiększenia wartości dokonana jest PO określeniu wartości
    * pre - operacja zwiększenia wartości dokonana jest PRZED określeniem wartości

np.
```java
int x = 5;
int y = x++;
```
> po wykonaniu operacji x ma wartosc 6, natomiast y wartosc 5,  
> ponieważ wartość x została zwiększona dopiero po przypisaniu do y

w przypadku
```java
int x = 5;
int y = ++x;
```
> po wykonaniu operacji obie zmienne mają wartość 6,   
> ponieważ wartość x została zwiększona przed przypisaniem do y

```java
post-inkrementacja: i++;
pre-inkrementacja: ++i;
```

* dekrementacja
    * post - operacja zmniejszenia wartości dokonana jest PO określeniu wartości
    * pre - operacja zmniejszenia wartości dokonana jest PRZED określeniem wartości

```java
post-dekrementacja: i--;
pre-dekrementacja: --i;
```

___Zadanie___
> Oswój się z operacjami matematycznymi, przetestuj każdą z nich w następujący sposób:
> zadeklaruj zmienne a i b, następnie przypisz wynik kolejnych operacji do zmiennych c,d,e,f
>> Oblicz średnią z tych liczb

___Zadanie 2___
> Oblicz pole i obwod koła w zależności od parametru r
>>  Przypisz wartości pola i obwodu do zmiennych. Jakiego typu mogą być te zmienne? 

___Zadanie 3___
> jak sprawdzić czy zadana liczba jest parzysta ?

___Zadanie 4___
> jaką wartość przyjmą zmienne x,y,z ?
```java
int x = 5; 
int y = ++x + x++;
int z = ++y;
```