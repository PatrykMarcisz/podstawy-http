### Obiektowe typy danych 
Każda definicja klasy, interfejsu czy enumu to typ danych.

Typ obiektowe to taka konstrukcja, która przechowuje stan (pola) i zachowanie (metody)

Zanim omówimy własne typy obiektowe, skoncentrujmy się na następującyh:
* Object
* String
* Obiektowe reprezentacje typów prymitywnych (Integer, Long, etc.)

####Co to jest klasa ?
Klasa to szablon na podstawie którego są tworzone obiekty posiadające te same cechy,   
ale różne wartości tych cech, np. butelka ;  
Każda butelka posiada zawartość oraz etykietę, jednakże jedna butelka może być butelką oranżady,  
 podczas gdy inna butelką wody mineralnej

### Object
Każda klasa ___dziedziczy___ po klasie Object. Oznacza to że każda inna klasa _dziedziczy_ ___zachowania___ klasy Object,  
 czyli ma dostęp do wszystkich metod które w klasie Object są zadeklarowane.

Te metody to między innymi:
* boolean equals(Object obj)    - metoda służąca do porównywania obiektów między sobą
* int hashCode()                - tzw. hash czyli ciąg liczb identyfikujący obiekt
* String toString()             - reprezentacja obiektu w formie ciągu znaków (String)

Aby utworzyć obiekt jakiegokolwiek typu należy użyć konstruktora, który zarezerwuje potrzebną na przechowywanie danego 
obiektu pamięć, oraz opcjonalnie wykona zdefiniowane przez nas operacje, takie jak np. ustalenie wartości pól obiektu.

w przypadku gdy żaden konstruktor nie zostanie zadeklarowany, zostanie użyty konstruktor domyślny,  
 bezargumentowy, którego struktura wygląda jak nazwa klasy z dodanymi nawiasami. 

```java
Object obj = new Object();
```

### String
String jak każda inna klasa posiada wspomniane wyżej metody, posiada również konstruktor, jednakże tym razem parametrowy.
Aby utworzyć nowy obiekt typu String korzystamy z następującej konstrukcji:

```java
String str = new String("Jakaś wartość");
```

W tym momencie pojawia się nowy typ literału - cudzysłów (choć jest zbliżone do literału typu char, np. ```'a'``` )
Jest jedna rzecz która odróżnia Stringa od innych obiektów - możemy utworzyć obiekt bez użycia konstruktora,  
 podając jedynie literał tak jak w przypadku typów prostych

```java
String str = "Jakaś wartość";
```

wynika to z faktu, że podczas kompilacji konstruktor wywoływany jest niejawnie
Ma to jeszcze dodatkowy efekt, ale może o tym innym razem

### Obiektowe typy prymitywne, tzw. Wrappery
Czy String to jedyny typ referencyjny (obiektowy ?) który posiada takie przywileje ? 
otóż nie, mamy jeszcze następujące typy obiektowe, odpowiadające typom prymitywnym:

| typ prymitywny | typ obiektowy |
| -------------- | ------------- |
| byte | Byte |
| short | Short |
| int | Integer |
| long | Long |
| float | Float |
| double | Double |
| boolean | Boolean |
| char | Character | 

typy te inicjuje się analogicznie  do typów prostych:

```java
Byte byteObject = 60;
Short shortObject = 90;
Integer integerObject = 240; 
Long longObject = 5500L; 
Float floatObject = 3.141592F; 
Double doubleObject = 2.718281828D; 
Character characterObject = 'a'; 
Boolean booleanObject = true; 
```

No może tak nie do końca analogicznie, bo jednak i tutaj dzieje się magia:

```java
Byte byteObject = new Byte(60); 
Short shortObject = new Short(90); 
Integer integerObject = new Integer(240); 
Long longObject = new Long(5500L); 
Float floatObject = new Float(3.141592F); 
Double doubleObject = new Double(2.718281828D); 
Character characterObject = new Character('a'); 
Boolean booleanObject = new Boolean(true); 
```

typy obiektowe w przeciwieństwie do typów prymitywnych posiadają metody, 
np. 
* doubleValue(), floatValue() itp. wynikiem ich działania są typy proste
* doubleObject(), floatObject() itp. wynikiem ich działania są typy referencyjne

#### Autoboxing i unboxing
##### autoboxing 
> automatyczna konwersja typów prostych na złożone. Tam, gdzie oczekiwany jest np. typ Integer możemy przekazać  
> np. wartość 5 lub zmienną typu int; zostanie ona automatycznie zamieniona na typ Integer. Podobnie z resztą typów.
##### unboxing
> automatyczna konwersja typów obiektowych na prymitywne. Tam, gdzie oczekiwany jest np. typ double możemy przekazać  
> zmienną typu Double. Zostanie ona automatycznie zamieniona na typ double. Podobnie z resztą typów


#### Co odróżnia typy obiektowe od typów prymitywnych ? 
Każdy z typów prymitywnych posiada wartość domyślną - zostało to wymienione w tabeli z typami prostymi.   
W przypadku typów obiektowych / referencyjnych, domyślną wartością jest null - specjalne miejsce w pamięci,  
 na które wskazuje każda niezainicjalizowana zmienna.

W przyszłych przygodach z programowaniem nie raz i nie dwa napotkacie na ekranie następujący komunikat:
```java
Exception in thread "main" java.lang.NullPointerException
```
występuje on zawsze wtedy, gdy będziemy chcieli wykonać jakąkolwiek operację na zmiennej, która wskazuje na ___null___ 