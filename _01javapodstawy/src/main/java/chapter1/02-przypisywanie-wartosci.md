> w Javie wartości zmiennych są przechowywane na dwa sposoby:
> * referencje (deklaracje) oraz wartości typów prostych trzymane są na stosie (stack),
> * nowotworzone obiekty umieszczane są na stercie (heap).

---
w momencie zadeklarowania zmiennej z określoną nazwą (np. a) otrzymujemy identyfikator,   
którym będziemy się posługiwać aby uzyskać wartość która się za nią "kryje".

```java
int a = 5;      // za każdym razem kiedy użyjemy wartości a,
                // będzie to tak jakbyśmy użyli wartości 5, 

System.out.println(a); //wydrukuje na ekran wartość 5;
```
---
zmienne mozemy nadpisywać, co wynika właściwie z nazwy; zmienne są ... zmienne :) 

```java
a = 50; //poprzednia wartość została zastąpiona nową 

System.out.println(a); //teraz na wydruku zobaczymy 50
```
---
jako że zmienne przechowują wartości, możemy przypisywać ich do innych zmiennych:

```java
int b = 30; 
int c = b;          // w przypadku typów prostych następuje kopiowanie wartości

b = 20;

System.out.println(b);  // wydruk  = 20
System.out.println(c);  // wydruk = 30;
```

Uwaga! nie możemy kilkakrotnie zdefiniować takiej samej nazwy zmiennej w ramach jednego zakresu widoczności   
(bloku { ... })

poniższy zapis jest niedozwolony
```java
  int b = 50;
  int b = 30;   //blad kompilacji, b już istnieje
```
---
Zastanówmy się jak wygląda operacja przypisania. W momencie gdy wykomujemy jakąś instrukcję,   
wykonywana jest najpierw operacja po prawej stronie operatora przypisania (czyli znaku równości = ),  
a dopiero wynik tej operacji jest zapisywany do zmiennej znajdującej się po lewej stronie operacji przypisania.
Dlatego w momencie gdy widzimy 
```java
int a = 10;
int b = a; 
```
w rzeczywistości jest wykonywana następna operacja: sprawdź co jest po prawej stronie operacji przypisania,   
oblicz wartość tego wyrażenia i przypisz do zmiennej której identyfikator znajduje się po lewej stronie

---
możliwe jest przypisanie zmiennej do innego typu, ale tylko i wyłącznie wówczas gdy 
* te typy są we wzajemnej relacji dziedziczenia (tzw. Is A) 
* w przypadku typów prostych - gdy następuje konwersja typu rozszerzającego (omówimy to w dalszej części)

##### podsumowując 
> wszędzie gdzie widzimy użycie zmiennej, widzimy jej typ oraz wartość 

---
#### ___Zadanie___

>zadeklaruj dwie zmienne: 
> ```java
> int zmienna1 = 10;  
> int zmienna2 = 20;
> ```
> następnie przy pomocy trzeciej zmiennej c, zamień ich wartości, tak aby zmienna2 miała wartość 10,  
> natomiast zmienna1 wartość 20 