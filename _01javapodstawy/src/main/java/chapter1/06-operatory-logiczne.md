### Operatory logiczne
Poza operatorami matematycznymi, w javie znajdziemy operatory logiczne, służące do operowania warunkami logicznymi  
 (być może niektórzy pamiętają ze szkoły średniej lub studiów tzw. logikę matematyczną)  
Te operatory to:
* koniunkcja
    * operator: &&
    * wynik: prawda tylko wówczas gdy wszystkie argumenty są prawdą
* alternatywa
    * operator: ||
    * wynik: prawda tylko wówczas, gdy chociaż jeden z argumentów jest prawdziwy
* negacja
    * operator: !
    * wynik: zaprzeczenie wartości której dotyczy
    
#### koniunkcja

Tabelka prawdy dla operacji ``&&``
 
| x | y | x AND y |
|-----|-----|-----|
| true | true | true |
| true | false |false | 
| false | true | false |
| false | false | false |


```java
int a = 5;
int b = 10;
boolean c = (a < 10) && (b > 5);    //jako że a jest mniejsze od 10, a b większe od 5, 
                                    // oba wyrażenia są prawdziwe, a ich koniunkcja (iloczyn) jest prawdą
```

```java
int a = 5;
int b = 10;
boolean c = (a < 5) && (b>10);      //fałsz, ponieważ a jest równe a nie mniejsze od 5, 
                                    // natomiast b jest równe a nie większe od 10
```

#### alternatywa 

Tabelka prawdy dla operacji ``||``
 
| x | y | x OR y |
|-----|-----|-----|
| true | true | true |
| true | false | true | 
| false | true | true |
| false | false | false |

Przykłady alternatywy:
```java
int a = 5;
int b = 10;
boolean c = (a == 5) || (b > 15); //prawda, poniewaz pierwszy warunek prawda
boolean d = (a > 10) || (b < 20); //prawda, poniewaz mimo ze pierwszy warunek fałsz, drugi prawda
boolean e = (a > 10) || (b > 20); //falsz, oba warunki nieprawdziwe
```

#### negacja
Tabelka prawdy dla negacji ``!``

| x | !x |
|-----|-----|
| true | false |
| false | true | 

```java
int a = 10;
boolean c = !(a > 20); //prawda, bo pomimo ze a > 20 daje falsz, negacja daje prawde
```