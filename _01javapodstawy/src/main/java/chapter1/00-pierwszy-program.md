### Pierwszy program

```java
package chapter1;

import java.lang.*;

public class PierwszyProgram {

    public static void main(String[] args) {

        System.out.println("Hello World");

    }

}
```

![obrazek opisujacy importy i pakiety](import-pakiet.png)

![obrazek opisujacy z czego składa się definicja klasy](definicja-klasy.png)

![obrazek opisujacy z czego składa się definicja metody](definicja-metody.png)

![obrazek opisujący z czego składa się instrukcja](definicja-instrukcji.png)


#### Zadanie
> Napisz program wypisujący "Hello World" na konsolę.

[](https://markdowntohtml.com/)