#### konwersja pomiędzy typami
wspomnieliśmy przed chwilą, że możliwa jest konwersja pomiędzy typami.  
W przypadku typów prostych (prymitywnych) rozróżniamy następujące rodzaje konwersji:
* rozszerzająca 
    > gdy konwertujemy typ o mniejszej pojemności do typu do większej, np. short do int, int do longa, float do double
* zwężająca 
    > gdy konwertujemy typ o większej pojemności do typu o mniejszej, np. long do inta, double do float

#### konwersja rozszerzająca
Przykład 1:

W poniższym przypadku mamy do czynienia z konwersją rozszerzającą, gdzie wartość o zakresie 2 bajtów,  
 umieszczamy w zmiennej mogącej przechowywać o wielkości 4 bajtów

```java
short wartoscShort = 10;
int wartoscInt = wartoscShort;
```

możliwa jest również konwersja typu int na typ double. Dzieje się tak, ponieważ double jest w stanie  
 pomieścić zarówno część całkowitą liczby, jak również część dziesiętną
```java
int dziesiecInt = 10;
double dziesiecDouble = dziesiecInt;
```

#### konwersja zwężająca
przeciwieństwem powyższych operacji jest konwersja zwężająca. Próbujemy wówczas "wcisnąć" wartość  
 o potencjalnie większą niż byłby w stanie przechować typ do którego zwężamy. 

np. w przypadku gdy chcemy umieścić liczbę z zakresu -32k, 32k z typu long do inta, wszystko jest w porządku,  
 jednakże dla kompilatora nie jest to oczywiste, wszak ta wartość może się tam nie zmieścić.

W takim przypadku musimy jawnie poinfrmować kompilator, że jesteśmy świadomi tej decyzji

```java
long dziesiecTysiecy = 10_000;
int dzieciecTysiecyInt = dziesiecTysiecy; 
```
kompilacja: 
``Error:(36, 34) java: incompatible types: possible lossy conversion from long to int``

poprawny zapis wygląda następująco:
```java
long dziesiecTysiecy = 10_000;
int dzieciecTysiecyInt = (int)dziesiecTysiecy;
```
---
sytuacja wygląda analogicznie dla rzutowania typu double na chociażby int:

```java
double pi = 3.14;
int piInt = pi;
```
kompilacja:
``Error:(39, 21) java: incompatible types: possible lossy conversion from double to int``

Kompilator wprost informuje nas: 
> hej, typ int trzyma liczby całkowite, a ty chcesz tam wcisnąć liczbę zmiennoprzecinkową,  
> możesz to zrobić, ale licz się z tym że stracisz część dziesiętną, upewnij mnie w tym że właśnie tego chcesz 

```java
double pi = 3.14;
int piInt = (int)pi; //wartosc piInt: 3
```

---

co w sytuacji gdy staramy się umieścić zbyt dużą wartość np. 130 w typie byte po zastosowaniu jawnej konwersji ?  
 no cóż, nie dostaniemy żadnego błędu, ale wynik może nas co najmniej zaskoczyć (np. tak jak twórców gry Civilization,  
  tzw. atomowy Ghandi)

```java
byte stoTrzydziesci = (byte)130;
```

#### ___Zadanie___
> * Przypisz 18 (literał) do zmiennych typu: byte, short, int, long, double, float.
> * Analogiczną operację wykonaj dla wartosci 35000 oraz 3_500_000_000
> 
> Dla jakich typów uda się te operacje, dla jakich trzeba zastosować dodatkowe operacje ?  
> jakie są rezultaty wydruku takich wartości