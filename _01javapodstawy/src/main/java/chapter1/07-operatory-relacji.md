### Operatory matematyczno - logiczne: operatory relacji

| Operacja | operator | 
|-----|-----|
|równy| == |
|różny| != |
|większy| > |
|większy lub równy| != |
|mniejszy| != |
|mniejszy lub równy| != |

Wynik działania każdego z tych operatorów to typ logiczny - boolean. 

```java
int a = 10;
int b = 20;
boolean c = a > b; //zwraca false, poniewaz 10 jest mniejsze od 20
```

do porównywania między sobą wartości używamy operatora == (równe) lub != (różne).  
 Warto jednak zaznaczyć, że taki tryb porównywania dotyczy głównie typów prymitywnych,  
  w przypadku typów obiektowych (o których jeszcze wspomimy) stosuje się głównie metodę .equals()

```java
int a = 10;
int b = 10;
boolean c = a == b; //prawda
```

możemy również przyrównywać wartości bezpośrednio do literałów
```java
int a = 10;
boolean b = a == 10; //prawda
```

> Typy prymitywny porównujemy przy pomocy operatora ==
>
> Typy obiektowe porównujemy przy pomocy metody equals ( == też jest dozwolone ale trzeba mieć świadomość jak działa)
>> np. przy sprawdzeniu czy dany obiekt ma wartość null, dozwolone jest użycie == 