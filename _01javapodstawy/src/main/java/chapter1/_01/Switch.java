package chapter1._01;

public class Switch {
    public static void main(String[] args) {
//        opisowka();

    }

    private static void opisowka() {
        int a = 5;
        switch(/* wartosc wyrazenia ktore jest typem liczbowym, znakowym, enumem lub Stringiem*/ a){
            case /*konkretna wartosc odpowiadająca wyrażeniu wyżej */ 1: {
                /* blok instrukcji który ma zostać wykonany jeżeli a jest równe 1 */
                System.out.println(1);
                break;  //instrukcja końca bloku, w innym przypadku kolejne przypadki również zostaną wykonane,
                        //aż do napotkania instrukcji break lub konca switch-a
            }
            case 2: {
                /* ... */
                break;
            }
            case 5: {
                /* ... */
                System.out.println(5);
                break;
            }
            default: {
                /* blok ktory wykona sie domyślnie jeśli nie znajdziemy dopasowania */
                /* nie potrzebny jest break */
            }
        } /* koniec instrukcji switch */
    }
}
