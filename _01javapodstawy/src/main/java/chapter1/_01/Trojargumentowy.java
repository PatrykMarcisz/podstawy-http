package chapter1._01;

public class Trojargumentowy {
    public static void main(String[] args) {
        jestPelnoletni(18);
    }

    private static void jestPelnoletni(int wiek) {

        String text = wiek >= 18 ? "pełnoletni" : "niepełnoletni";
        text = (wiek > 18) ? "pełnoletni" : "niepełnoletni";
        text = wiek > 18
                ? "pełnoletni"
                : "niepełnoletni";

        boolean jestPelnoletni = wiek >= 18 ? true : false;
        jestPelnoletni = wiek >= 18;
        jestPelnoletni = sprawdzWiek() ? true : false;
        jestPelnoletni = sprawdzWiek();

        int liczba = 10;
        String jestParzysta = liczba % 2 == 0 ? "parzysta" : "nieparzysta";
    }

    private static boolean sprawdzWiek() {
        return false;
    }


}
