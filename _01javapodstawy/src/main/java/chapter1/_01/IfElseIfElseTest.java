package chapter1._01;

import java.util.Scanner;

public class IfElseIfElseTest {

    public static void main(String[] args) {
        int i = pobierzLiczbe();
        wiekszyMniejszyLubRowny(i);
    }

    private static int pobierzLiczbe() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static void wiekszyMniejszyLubRowny(int i) {

        if (i > 0) {
            System.out.println("liczba " + i + " jest większa od 0");
        } else if (i < 0) {
            System.out.println("Liczba " + i + " jest mniejsza od 0");
        } else {
            System.out.println("Liczba " + i + " jest równa 0");
        }

    }
}
