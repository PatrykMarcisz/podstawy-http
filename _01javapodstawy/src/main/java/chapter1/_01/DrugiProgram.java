package chapter1._01;

public class DrugiProgram {
    public static void main(String[] args) {
        //przypisz 18 do zmiennych typu: byte, short, int, long, double, float.
        //nastepnie zrob to samo dla wartosci 35000 oraz 3_500_000_000
        // Dla jakich typów uda się te operacje, dla jakich trzeba zastosować dodatkowe operacje ? jakie są rezultaty wydruku takich wartości

        byte wartoscByte = (byte)3_500_000_000L;
        short wartoscShort = (short)3_500_000_000L;
        int wartoscInt = (int)3_500_000_000L;
        long wartoscLong = 3_500_000_000L;
        double wartoscDouble = 3_500_000_000L;
        float wartoscFloat = 3_500_000_000L;

        System.out.println(wartoscByte);
        System.out.println(wartoscShort);
        System.out.println(wartoscInt);
        System.out.println(wartoscLong);
        System.out.println(wartoscDouble);
        System.out.println(wartoscFloat);

        //dla typow dziesietnych (double, float) spróbuj przypisac wartosci: (do dopracowania)
        double double2 = 1e-323;
        float float2 = 1e-45f;

        int b = 30;
        int c = b;          // w przypadku typów prostych następuje kopiowanie wartości

        b = 20;

        System.out.println(b);  // wydruk = 20
        System.out.println(c);  // wydruk = 30;

        long dziesiecTysiecy = 10_000;
        int dzieciecTysiecyInt = (int)dziesiecTysiecy;

        double pi = 3.14;
        int piInt = (int)pi;
        method();
        method2();
    }

    private static void method2() {
        int x = 5;
        int y = x++;
        System.out.println(x);
        System.out.println(y);
    }

    static void method(){
        int a = 10;
        int b = 20;
        double c = a/(double)b;
        System.out.println(c);

        int x = 5; //w momencie inicjalizacji 5
        System.out.println("x " + x);

        int y = ++x + x++;
        System.out.println("y " + y);

        int z = ++y;
        System.out.println("z " + z);

        System.out.println("x2 " + x);
        System.out.println("y2 " + y);
        System.out.println("z2 " + z);

    }

    public static void ifElse(int x){

        if( x > 0 ) {
            System.out.println("x > 0");
        } else {
            System.out.println("x < 0");
        }

    }
}
