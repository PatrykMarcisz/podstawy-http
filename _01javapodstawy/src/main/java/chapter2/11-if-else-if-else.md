#### if-else-if-else

```java
    if (i > 0) {
        System.out.println("liczba " + i + " jest większa od 0");
    } else if (i < 0) {
        System.out.println("Liczba " + i + " jest mniejsza od 0");
    } else {
        System.out.println("Liczba " + i + " jest równa 0");
    }
```

![](if-else-if-else.png)

Konstrukcję if - else-if - else-if ... - else wykorzystujemy wówczas, gdy mamy więcej niż jedną ścieżkę wykonania programu 
(np. trzy warianty wykonania operacji w zależności od wartości parametru delta przy obliczaniu miejsc zerowych funkcji kwadratowej)



#### ___Zadanie___
> Sprawdź czy podana przez użytkownika liczba jest podzielna przez 3 - wypisz "Podzielna przez 3",  
> jeśli przez 7 - "Podzielna przez 7",   
> jeśli przez 3 i 7 - "Podzielna przez 3 i 7".

### ___Zadanie 2___
> Rozbuduj program sprawdzający czy dany znak jest literą alfabetu na taki, który będzie określał czy  
> dany znak jest literą, cyfrą czy innym znakiem

