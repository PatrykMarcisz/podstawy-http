#### for

Poniższe zadania wykonaj dla każdego z rodzajów pętli:

##### ___Zadanie___
> Wypisz wszystkie liczby z zakresu 1 ... n

##### ___Zadanie___
> Wypisz wszystkie liczby z zakresu n ... 1 (malejąco)

##### ___Zadanie 3___
> Wypisz wszystkie liczby z zakresu:
> * 0 - 100.  
> * 11-77.  
> * 0 - 100 malejąco.

##### ___Zadanie 4___
> Przy pomocy pętli for, wypisz wszystkie litery a - z

##### ___Zadanie 5____
> Oblicz sumę cyfr zadanej liczby

##### ___Zadanie 6___
> rozszerz zadanie 5 o sumowanie tak długo, aż zostanie jedna cyfra

##### ___Zadanie 7____
> napisz program, który przyjmuje od użytkownika zakres liczb.   
>Następnie wypisz ilość liczb parzystych, nieparzystych, ich sumę, sumę wszystkich liczb oraz średnią

##### ___Zadanie 8____
> Stwórz program, który tworzy kwadrat ze znaku hash "#" 
> o długości boku, który zostanie podany przez użytkownika. 
>> (Dla chętnych gwiazdka - niech kwadrat będzie pusty w środku, czyli tylko boki są narysowane).