#### Switch
W niektórych przypadkach warto rozważyć zastąpienie ciągu instrukcji if-else inną konstrukcją - ```switch```

```java
        int a = 5;
        switch(/* wartosc wyrazenia ktore jest typem liczbowym, znakowym, enumem lub Stringiem*/ a){
            case /*konkretna wartosc odpowiadająca wyrażeniu wyżej */ 1: {
                /* blok instrukcji który ma zostać wykonany jeżeli a jest równe 1 */
                System.out.println(1);
                break;  //instrukcja końca bloku, w innym przypadku kolejne przypadki również zostaną wykonane,
                        //aż do napotkania instrukcji break lub konca switch-a
            }
            case 2: {
                /* ... */
                break;
            }
            case 5: {
                /* ... */
                System.out.println(5);
                break;
            }
            default: {
                /* blok ktory wykona sie domyślnie jeśli nie znajdziemy dopasowania */
                /* nie potrzebny jest break */
            }
        } /* koniec instrukcji switch */
```

##### ___Zadanie___
> Napisz program który wyświetli nazwę dnia tygodnia w zależności od liczby przekazanej do switcha  
> w przypadku liczby spoza zakresu <1-7> wypisz komunikat "Nie ma takiego dnia"

##### ___Zadanie 2___
> Napisz program, który wyświetli ilość dni w miesiącu, w zależności od numeru miesiąca

##### ___Zadanie 3___
> Wykonaj powyższe zadania w wariancie if-else-if-else

##### ___Zadanie 4___
> Przy pomocy instrukcji switch napisz prosty kalkulator, gdzie argumentem switch jest znak operacji ( +, -, *, /)