#### operator-trójargumentowy
Operator trójargumentowy ?: jest to instrukcja działająca prawie identycznie jak konstrukcja ``if-else`` z tą różnicą, 
że jest to operator a nie instrukcja sterująca, co powoduje, że kompilator stosuje inne reguły walidacji.   
Operator skraca dość znacznie zapis jednak jest mniej czytelny niż konstrukcja if else.

###### składnia:
```java
    <wyrażenie-logiczne> ? <wartość-1> : <wartość-2>
```

###### przykład:
```java
        String text = wiek >= 18 ? "pełnoletni" : "niepełnoletni";

        text = (wiek > 18 ) ? "pełnoletni" : "niepełnoletni";

        text = wiek > 18 
                ? "pełnoletni"
                : "niepełnoletni";
        
        //jako if-else
        if(wiek>=18){
            text = "pełnoletni";    
        } else {
            text = "niepełnoletni";
        }
```

```java
        boolean jestPelnoletni = wiek >= 18 ? true : false;

        jestPelnoletni = wiek >= 18;

        jestPelnoletni = sprawdzWiek() ? true : false;

        jestPelnoletni = sprawdzWiek();
```

```java
        int liczba = 10;

        String jestParzysta = liczba % 2 == 0 ? "parzysta" : "nieparzysta";
```

Uwaga!
```java
<wyrażenie-logiczne> ? <wartość-1> : <wartość-2>
```
Zwykle ``wartość-1`` i ``wartość-2`` są tego samego typu, np. int, czy String. Natomiast w poniższym przykładzie
```java
System.out.println(true ? 1 : 3.1415);
```
Mimo iż wyrażenie logiczne ma wartość true, to na ekranie wyświetlona zostanie wartość 1.0 a nie 1 - jakby się można początkowo spodziewać.  
Operator trójargumentowy działa tak, że typem zwracanym jest wspólny (najwęższy) typ, w tym przypadku double.  
Jeśli chcielibyśmy przypisać wynik tego działania moglibyśmy napisać np. tak:
```java
double result = true ? 1 : 3.1415;
```