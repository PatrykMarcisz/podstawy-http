##### Instrukcja if-else

córka konstrukcji if - wzbogacona o blok instrukcji, jaki ma zostać wykonany w przypadku gdy wartość warunku ma wartość false

```java

    if( warunek-logiczny ) {
        operacja1;
        operacja2;
        operacja3;   
    } else {
        operacja4;
        operacja5;
        operacja6;
    }   

```
![if-else](if-else.png)

przykłady:
```java
int x = 0;
if(x > 0){
    System.out.println("x > 0");
} else {
    System.out.println("x < 0");
}
```
#### ___Zadanie___
> Zadeklaruj 2 zmienne typu całkowitego i przypisz im wartości. Wypisz wartość większej na konsolę.

#### ___Zadanie 2___
> Zadeklaruj zmienną typu całkowitego i sprawdź czy jest parzysta. Wypisz odpowiedni komunikat.

#### ___Zadanie 3___
> Napisz program który sprawdzi czy dany znak należy do alfabetu, czy też nie. Wykorzystaj do tego możliwość rzutowania  
> znaku char na int (int)a gdzie a - char, oraz tablice ASCII

#### ___Zadanie 4___
> Napisz program, który sprawdzi czy z podanej długości boków a,b,c można zbudować trójkąt

#### ___Zadanie 5___
> Jeżeli jest możliwe zbudowanie trójkąta, spróbuj określić czy trójkąt jest trójkątem równobocznym,  
> równoramiennym, prostokątnym czy żadnym z powyższych

#### ___Zadanie 6___
> Napisz program, który w zależności od wyniku testu przypisze ocenę:
> >=90% : ocena 5  
> >=80% : ocena 4.5  
> >=70% : ocena 4  
> >=60% : ocena 3.5  
> >=50% : ocena 3  
> <=50% : ocena 2  
> Na wejściu otrzymujemy ilość punktów dla danego studenta oraz maksymalną ilość dostępnych punktów.
> Zabezpiecz program przed możliwością podania większej ilości punktów studenta niż maksymalna ilość punktów do zdobycia

