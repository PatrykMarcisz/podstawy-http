public class For {
    public static void main(String[] args) {
        int start = 1;
        int koniec = 10;
        for (int i = start; i <= koniec; i++) {
            for (int j = 1; j <= 10; j++) {
                System.out.print(i + "x" + j + "=" + i * j + "\t");
            }
            System.out.println();
        }

        //choinka
        int n = 5;
        for(int i= 0; i <= n; i++){
            for(int j = 0; j < i; j++){
                System.out.print("*");
            }
            System.out.println();
        }

        int i = 0;
        while (i <= -10) {

            i++;
        }
    }
}
