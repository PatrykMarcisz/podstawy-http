public class DoWhile {
    public static void main(String[] args) {
        //<65, 90> duze litery
        char A = (char)65;
        System.out.println(A);
        char Z = (char)90;
        System.out.println(Z);

        int aInt = 65;
        char fromInt = (char)aInt;
        char fromMethod = (char)daj65();

        do {
            System.out.println((char)aInt++);
        } while(aInt <= 90);

        int licznik  = 65;
        do {
            char znakZLiczby = (char) licznik;
            System.out.println(znakZLiczby);
            licznik++;
        } while(licznik <= 90);

    }

    private static int daj65() {
        return 65;
    }
}
