public class TrojkatMozna {
    public static void main(String[] args) {
        double a = 40;
        double b = 20;
        double c = 10;

        boolean pierwszyWarunek = a + b > c;
        boolean drugiWarunek = a + c > b;
        boolean trzeciWarunek = b + c > a;

        if (a + b > c && drugiWarunek && trzeciWarunek) {
            System.out.println("można zbudować trojkat");
            boolean z = false;
        } else {
            System.out.println("nie mozna zbudowac trojkata");
            boolean z = true;
        }

//        wynik = (wyrazenie) ? ...jesli prawda... : ...jesli falsz;

        // text = wiek > 18
        //                ? "pełnoletni"
        //                : "niepełnoletni";

        String wynik = (pierwszyWarunek && drugiWarunek && trzeciWarunek)
                ? "można zbudować trojkat"
                : "nie mozna zbudowac trojkata";
        System.out.println(wynik);

    }
}