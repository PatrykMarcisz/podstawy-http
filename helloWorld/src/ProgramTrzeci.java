public class ProgramTrzeci {
    public static void main(String[] args) {
        short a = 40;
        int b = a;
        /*
            to jest
            komentarz wielolinijkowy
         */
        int c = 40;
        float e = 3.34f;
        short d = (short)c;

        int zmienna1 = 10;
        int zmienna2 = 20;
        System.out.println(zmienna1);
        System.out.println(zmienna2);

        int zmienna3;
        zmienna3 = zmienna1;
        zmienna1 = zmienna2;
        zmienna2 = zmienna3;


        System.out.println(zmienna1);
        System.out.println(zmienna2);

        byte stoPiecdziesiat = (byte)150;
        System.out.println(stoPiecdziesiat);

        int costam = 5;
        byte innycos = (byte)costam;
        System.out.println(innycos);

        //byte, short, int, long, double, float.
        //35000 oraz 3_500_000_000
        byte b1 = (byte)35000;
        short s1 = (short)35000;
        int i1 = 35000;
        long l1 = 35000;
        double d1 = 35000;
        float f1 = 35000;

        byte b2 = (byte)3_500_000_000L;
        short s2 = (short)3_500_000_000L;
        int i2 = (int)3_500_000_000L;
        long l2 = 3_500_000_000L;
        double d2 = (double)3_500_000_000L;
        float f2 = (float)3_500_000_000L;
    }
}
