import java.lang.Math;

public class OperacjeMatematyczne {
    //String[] args
    //String args[]
    //String []args

    public static void main(String[] args) {
        int a = 2_147_483_647;
        int b = 1;

        long c = (long)a + (long)b;
        System.out.println(c);
        System.out.println((long)a + (long)b);

        short s1 = (short)10;
        short s2 = (short)15;

        short s = (short)(s1 + s2);

        double dziel1 = 10;
        double dziel2 = 20;
        int dziel3 = (int)(dziel1 / dziel2);
        System.out.println(dziel3);

        int resztaZDzielenia = 16%15;
        System.out.println(resztaZDzielenia);

        String str = 500 + "";
        int itr = Integer.parseInt(str);

        double pi = Math.PI;
        double r = 40.0;
        double pole = pi * Math.pow(r, 2);
        double obwod = 2 * pi * r;

        int a1 = 231;
        double b1 = 23.0;
        int c1 = (int)(a1 * b1);
        double c2 = a1 * b1;
        double d = a1 * b1 / c2;
        double srednia = (a1+b1+c1+c2+d)/5;
        inkrementacja(c1, 2, 3);
        inkrementacja(3, 4, 5);
        System.out.println(srednia);
    }

    public static void inkrementacja(int a, int b, int c) {
        System.out.println("-----------");
        int x = 0;
        x++; //teraz x = 1, to samo co x = x + 1;
        System.out.println(x);

        ++x; //teraz x = 2,
        System.out.println(x);

        int y = x++; //y = 2, x = 3;
        System.out.println(y);
        System.out.println(x);

        int z = ++x; // z = 4, x = 4;
        System.out.println(z);
        System.out.println(x);
        System.out.println("######");

        int x1 = 5;
        System.out.println(x1);
        int y1 = ++x1 + x1++;
        //x = x+1;
        System.out.println(x1);
        System.out.println(y1);
        int z1 = ++y1;
        System.out.println(y1);

        System.out.println(x1 + " " + y1 + " " + z1);

        kolejnaFunkcja();

    }

    private static void kolejnaFunkcja() {
        int x = 5;
        System.out.println(x);
        x += 5;
        System.out.println(x);
        x -= 3;
        System.out.println(x);
        x *= 10;
        System.out.println(x);
        x /= 2;
        System.out.println(x);
        x += x;
        System.out.println(x);
        x /= 7;
        System.out.println(x);
        x += x + 15;
        System.out.println(x);
        x += x++;
        System.out.println(x);
        x += ++x;
        System.out.println(x);
    }

}
