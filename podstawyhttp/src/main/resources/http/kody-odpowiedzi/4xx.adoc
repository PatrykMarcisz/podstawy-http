== Statusy 4xx

|===
|Kod |Znaczenie |Przeznaczenie

|4xx
|
|Błędy klienta (aplikacji)

|400
|BAD REQUEST
|Ogólny kod błędu

|401
|UNAUTHORIZED
|wymagane jest uwierzytelnienie

|403
|FORBIDDEN
|wymagana jest autoryzacja, np. hasło gdy użytkownik niezalogowany

|404
|NOT FOUND
|nie znaleziono zasobu

|405
|METHOD NOT ALLOWED
|metoda http nieobsługiwana, np. DELETE

|406
|NOT ACCEPTABLE
|dane wysłane przez klienta są niepoprawne

|408
|REQUEST TIMEOUT
|serwer przekroczył limit czasu oczekiwania na pełne żądanie przeglądarki

|415
|UNSUPPOORTED MEDIA TYPE
|Dane wysyłane przez klienta w niewspieranym przez nas formacie (np. XML gdy obsługiwany tylko json)

|429
|TOO MANY REQUESTS
|użytkownik wysłał zbyt wiele żądań w określonym czasie
|===
