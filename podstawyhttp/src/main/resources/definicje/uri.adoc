== URI
Uniform Resource Identifier

http://www.example:8080/katalog1/katalog2/plik?parametr1=wartosc1&parametr2=wartosc2#fragment_dokumentu
(przerobić na obrazek)
protokół: http
host(adres serwera) : www.example.com
8080 - port (domyślny dla http: 80)
katalog1/katalog2/plik - zasób
parametr1=wartosc1&parametr2=wartosc2 - parametry URL
#fragment_dokumentu - odnośnik na stronie (niby pisze fragment dokumentu, ale kto wie)