package software.development.academy.podstawy.http.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import software.development.academy.podstawy.http.dto.Author;
import software.development.academy.podstawy.http.dto.AuthorWithBooks;
import software.development.academy.podstawy.http.dto.Book;
import software.development.academy.podstawy.http.repository.dao.AuthorEntity;
import software.development.academy.podstawy.http.repository.AuthorRepository;
import software.development.academy.podstawy.http.repository.BookRepository;
import software.development.academy.podstawy.http.repository.dao.BookEntity;
import software.development.academy.podstawy.http.service.converter.AuthorWithBooksConverter;
import software.development.academy.podstawy.http.service.converter.BookConverter;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AuthorService {
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;

    @Autowired
    public AuthorService(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    public List<Book> getBooksForAuthor(Long id) {
        return authorRepository
                .findById(id)
                .filter(x -> Objects.nonNull(x.getBooks()))
                .filter(x -> x.getBooks().size() > 0)
                .map(AuthorEntity::getBooks)
                .map(bookEntities -> bookEntities
                        .stream()
                        .map(Book::new)
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }

    public List<AuthorWithBooks> allAuthors() {
        return authorRepository.findAll().stream()
                .map(AuthorWithBooksConverter::fromEntity)
                .collect(Collectors.toList());
    }

    public Author authorById(Long id) {
        return new Author(authorRepository.getOne(id));
    }

    public Author saveAuthor(Author author) {
        AuthorEntity entity = author.toEntity();
        Optional<AuthorEntity> authorInBase = authorRepository.findOne(Example.of(entity));
        if (authorInBase.isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "author already exists");
        }
        AuthorEntity save = authorRepository.save(entity);
        return new Author(save);
    }

    public Author updateAuthor(AuthorWithBooks author, Long id) {
        Optional<AuthorEntity> entity = authorRepository.findById(id);
        if (entity.isPresent()) {
            return updateAuthorEntityUsingDto(author, entity.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "cannot update not existing item");
        }
    }

    private Author updateAuthorEntityUsingDto(AuthorWithBooks author, AuthorEntity entityInDatabase) {
        entityInDatabase.setBirthYear(author.getBirthYear());
        entityInDatabase.setName(author.getName());
        entityInDatabase.setSurname(author.getSurname());
        return new Author(entityInDatabase);
    }


    public Author updateAuthorPartially(Long id, Author body) {
        Optional<AuthorEntity> entity = authorRepository.findById(id);
        if (entity.isPresent()) {
            return updatePartially(body, entity.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "cannot update not existing item");
        }
    }

    private Author updatePartially(Author author, AuthorEntity entityInDatabase){
        if(Objects.nonNull(author.getName())){
            entityInDatabase.setName(author.getName());
        }
        if(Objects.nonNull(author.getSurname())){
            entityInDatabase.setSurname(author.getSurname());
        }
        if(Objects.nonNull(author.getBirthYear())){
            entityInDatabase.setBirthYear(author.getBirthYear());
        }
        return new Author(entityInDatabase);
    }

    public AuthorWithBooks addBookForAuthor(Long id, Book book) {
        Optional<AuthorEntity> authorEntity = authorRepository.findById(id);
        if(authorEntity.isPresent()){
            BookEntity bookAsEntity = BookConverter.toEntity(book);
            Optional<BookEntity> searchResult = bookRepository.findOne(Example.of(bookAsEntity));
            if(!searchResult.isPresent()){
                bookRepository.save(bookAsEntity);
            }
            authorEntity.get().getBooks().add(bookAsEntity);
            return AuthorWithBooksConverter.fromEntity(authorEntity.get());
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "cannot find author with provided id");
    }
}
