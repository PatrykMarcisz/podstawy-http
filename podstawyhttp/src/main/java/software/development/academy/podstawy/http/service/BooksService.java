package software.development.academy.podstawy.http.service;

import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import software.development.academy.podstawy.http.dto.Author;
import software.development.academy.podstawy.http.dto.Book;
import software.development.academy.podstawy.http.dto.BookWithAuthors;
import software.development.academy.podstawy.http.repository.dao.AuthorEntity;
import software.development.academy.podstawy.http.repository.AuthorRepository;
import software.development.academy.podstawy.http.repository.dao.BookEntity;
import software.development.academy.podstawy.http.repository.BookRepository;
import software.development.academy.podstawy.http.service.converter.BookWithAuthorsConverter;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class BooksService {
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;

    public BooksService(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    public List<BookWithAuthors> getAllBooks() {
        return bookRepository
                .findAll().stream()
                .map(BookWithAuthorsConverter::fromEntity)
                .collect(Collectors.toList());
    }

    public Book getBookById(Long id) {
        return new Book(bookRepository.getOne(id));
    }




    public BookWithAuthors saveBook(BookWithAuthors book) {
        List<Author> authors = book.getAuthors();
        List<AuthorEntity> authorEntities = mergeWithExistings(authors);
        BookEntity bookEntity = book.toEntity(authorEntities);

        Optional<BookEntity> searchBookResult = bookRepository.findOne(Example.of(bookEntity));
        if (searchBookResult.isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "book already exists");
        }

        BookEntity savedBook = bookRepository.save(bookEntity);
        authorEntities.forEach(x -> {
            if (x.getBooks() == null) {
                x.setBooks(new ArrayList<>());
            }
            x.getBooks().add(bookEntity);
            authorRepository.save(x);
        });

        return BookWithAuthorsConverter.fromEntity(savedBook);
    }

    public BookWithAuthors updateBook(Long id, BookWithAuthors book) {
        Optional<BookEntity> optionalEntity = bookRepository.findById(id);
        if (optionalEntity.isPresent()) {
            BookEntity entity = updateBookEntityUsingDto(book, optionalEntity);
            return BookWithAuthorsConverter.fromEntity(entity);
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "cannot find book with provided id");
    }

    private List<AuthorEntity> mergeWithExistings(List<Author> authors) {
        if(Objects.isNull(authors)){
            return Collections.emptyList();
        }
        return authors.stream().map(Author::toEntity).map(x -> {
            Example<AuthorEntity> of = Example.of(x);
            Optional<AuthorEntity> searchAuthorResult = authorRepository.findOne(of);
            return searchAuthorResult.orElse(x);
        }).collect(Collectors.toList());
    }

    private BookEntity updateBookEntityUsingDto(BookWithAuthors book, Optional<BookEntity> optionalEntity) {
        BookEntity entity = optionalEntity.get();
        entity.setPages(book.getPages());
        entity.setTittle(book.getTittle());
        List<AuthorEntity> authorEntities = mergeWithExistings(book.getAuthors());
        entity.setAuthorEntities(authorEntities);
        return entity;
    }


}
