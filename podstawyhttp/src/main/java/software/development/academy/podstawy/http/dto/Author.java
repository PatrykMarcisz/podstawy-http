package software.development.academy.podstawy.http.dto;

import software.development.academy.podstawy.http.repository.dao.AuthorEntity;

public class Author {
    private Long id;
    private String name;
    private String surname;
    private Integer birthYear;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Author() {
    }

    public Author(AuthorEntity authorEntity) {
        this.id = authorEntity.getId();
        this.name = authorEntity.getName();
        this.surname = authorEntity.getSurname();
        this.birthYear = authorEntity.getBirthYear();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }

    public AuthorEntity toEntity() {
        AuthorEntity entity = new AuthorEntity();
        entity.setName(this.name);
        entity.setSurname(this.surname);
        entity.setBirthYear(this.birthYear);
        return entity;
    }
}
