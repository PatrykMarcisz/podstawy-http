package software.development.academy.podstawy.http.service.converter;

import software.development.academy.podstawy.http.dto.AuthorWithBooks;
import software.development.academy.podstawy.http.dto.Book;
import software.development.academy.podstawy.http.repository.dao.AuthorEntity;

import java.util.List;
import java.util.stream.Collectors;

public class AuthorWithBooksConverter {

    public static AuthorWithBooks fromEntity(AuthorEntity authorEntity) {
        AuthorWithBooks author = new AuthorWithBooks();
        author.setId(authorEntity.getId());
        author.setName(authorEntity.getName());
        author.setSurname(authorEntity.getSurname());
        author.setBirthYear(authorEntity.getBirthYear());
        List<Book> books = authorEntity.getBooks().stream()
                .map(Book::new)
                .collect(Collectors.toList());
        author.setBooks(books);
        return author;
    }
}
