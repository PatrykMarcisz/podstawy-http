package software.development.academy.podstawy.http.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import software.development.academy.podstawy.http.repository.dao.AuthorEntity;

public interface AuthorRepository extends JpaRepository<AuthorEntity, Long> {
}
