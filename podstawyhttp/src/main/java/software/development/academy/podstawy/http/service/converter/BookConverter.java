package software.development.academy.podstawy.http.service.converter;

import software.development.academy.podstawy.http.dto.Book;
import software.development.academy.podstawy.http.repository.dao.BookEntity;

public class BookConverter {
    public static BookEntity toEntity(Book book){
        BookEntity entity = new BookEntity();
        entity.setPages(book.getPages());
        entity.setTittle(book.getTittle());
        return entity;
    }
}
