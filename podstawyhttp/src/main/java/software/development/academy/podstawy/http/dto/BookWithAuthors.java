package software.development.academy.podstawy.http.dto;

import java.util.List;

public class BookWithAuthors extends Book {

    private List<Author> authors;

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }
}
