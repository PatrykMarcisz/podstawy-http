package software.development.academy.podstawy.http.service.converter;

import software.development.academy.podstawy.http.dto.Author;
import software.development.academy.podstawy.http.dto.BookWithAuthors;
import software.development.academy.podstawy.http.repository.dao.BookEntity;

import java.util.List;
import java.util.stream.Collectors;

public class BookWithAuthorsConverter {

    public static BookWithAuthors fromEntity(BookEntity savedBook) {
        BookWithAuthors bookWithAuthors = new BookWithAuthors();
        bookWithAuthors.setId(savedBook.getId());
        bookWithAuthors.setTittle(savedBook.getTittle());
        bookWithAuthors.setPages(savedBook.getPages());
        List<Author> authorsList = savedBook.getAuthorEntities().stream().map(Author::new).collect(Collectors.toList());
        bookWithAuthors.setAuthors(authorsList);
        return bookWithAuthors;
    }

}
