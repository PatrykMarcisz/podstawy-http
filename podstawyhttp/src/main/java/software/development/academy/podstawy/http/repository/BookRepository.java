package software.development.academy.podstawy.http.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import software.development.academy.podstawy.http.repository.dao.BookEntity;

public interface BookRepository extends JpaRepository<BookEntity, Long> {

}
