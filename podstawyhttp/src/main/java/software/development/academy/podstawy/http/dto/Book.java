package software.development.academy.podstawy.http.dto;

import software.development.academy.podstawy.http.repository.dao.AuthorEntity;
import software.development.academy.podstawy.http.repository.dao.BookEntity;

import java.util.List;

public class Book {
    private Long id;
    private String tittle;
    private Integer pages;

    public Book() {
    }

    public Book(BookEntity entity) {
        this.id = entity.getId();
        this.tittle = entity.getTittle();
        this.pages = entity.getPages();
    }

    public BookEntity toEntity(List<AuthorEntity> authors) {
        BookEntity entity = new BookEntity();
        entity.setAuthorEntities(authors);
        entity.setTittle(this.tittle);
        entity.setPages(this.pages);
        return entity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }
    
    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }
}
