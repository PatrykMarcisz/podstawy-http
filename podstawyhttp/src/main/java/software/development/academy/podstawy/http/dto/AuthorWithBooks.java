package software.development.academy.podstawy.http.dto;

import java.util.List;

public class AuthorWithBooks extends Author{

    private List<Book> books;

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
