package software.development.academy.podstawy.http.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import software.development.academy.podstawy.http.dto.Author;
import software.development.academy.podstawy.http.dto.AuthorWithBooks;
import software.development.academy.podstawy.http.dto.Book;
import software.development.academy.podstawy.http.service.AuthorService;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@Transactional
public class AuthorController {

    private final AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping(value = "/authors", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<AuthorWithBooks> authorList() {
        return authorService.allAuthors();
    }

    @GetMapping(value = "/authors/{id}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Author getAuthorById(@PathVariable("id") Long id) {
        return authorService.authorById(id);
    }

    @GetMapping(value = "/authors/{id}/books", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<Book> getBooksForAuthor(@PathVariable("id") Long id) {
        return authorService.getBooksForAuthor(id);
    }

    @PostMapping(value = "/authors", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Author addAuthor(@RequestBody AuthorWithBooks author) {
        return authorService.saveAuthor(author);
    }

    @PostMapping(value = "/authors/{id}/books")
    public AuthorWithBooks addBooks(@RequestBody Book book, @PathVariable("id") Long id){
        return authorService.addBookForAuthor(id, book);
    }

    @PutMapping(value = "/authors/{id}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Author updateAuthor(@RequestBody AuthorWithBooks body, @PathVariable("id") Long id) {
        return authorService.updateAuthor(body, id);
    }

    @PatchMapping(value = "authors/{id}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Author updatePartially(@RequestBody Author body, @PathVariable("id") Long id){
        return authorService.updateAuthorPartially(id, body);
    }

}
