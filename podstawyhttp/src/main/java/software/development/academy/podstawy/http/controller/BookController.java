package software.development.academy.podstawy.http.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import software.development.academy.podstawy.http.dto.Book;
import software.development.academy.podstawy.http.dto.BookWithAuthors;
import software.development.academy.podstawy.http.service.BooksService;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@Transactional
public class BookController {

    private final BooksService booksService;

    @Autowired
    public BookController(BooksService booksService) {
        this.booksService = booksService;
    }

    @GetMapping("/books")
    public List<BookWithAuthors> getBooks() {
        return booksService.getAllBooks();
    }

    @GetMapping(value = "/books/{id}",
            produces = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE
            }
    )
    public Book getBook(@PathVariable("id") Long id) {
        return booksService.getBookById(id);
    }

    @PostMapping("/books")
    public Book addBook(@RequestBody BookWithAuthors book) {
        return booksService.saveBook(book);
    }

    @PutMapping("/books/{id}")
    public BookWithAuthors updateBook(@RequestBody BookWithAuthors book, @PathVariable("id") Long id){
        return booksService.updateBook(id, book);
    }

}
